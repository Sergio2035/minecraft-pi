from mcpi.minecraft import Minecraft
from mcpi import block
from   time import sleep
import random
import math
pi = math.pi
sin = math.sin
cos = math.cos

def init():
	ipString = "10.183.3.12"
	#ipString = "10.183.3.67"
	#mc = Minecraft.create("127.0.0.1", 4711)
	mc = Minecraft.create(ipString, 4711)
	#mc.setting("world_immutable",False)
	mc.setting("world_immutable",True)
	#x, y, z = mc.player.getPos()  
	return mc

def check(mc,x,y,z):
	air = 0
	mc.setBlocks(-127,0,-127,128,0,128,air)
	mc.setBlocks(x,y-10,z,x,y+2,z,35,15)# 0,y,0 black
	mc.setBlocks(x+5,y-10,z+5,x+5,y+127,z+5,35,14) #red
	mc.setBlocks(x+5,y-10,z-5,x+5,y+127,z-5,35,5) #green
	mc.setBlocks(x-5,y-10,z-5,x-5,y+127,z-5,35,3) #blue
	mc.setBlocks(x-5,y-10,z+5,x-5,y+127,z+5,35,4) #yellow
	
'''
wool 35,3  LIGHT BLUE 
wool 35,4  YELLOW
wool 35,5  GREEN
wool 35,14  RED

'''
def obj1(mc,x,y,z):
	mc.setBlocks(x,y-10,z,x,y+2,z,45)# 0,y,0 black

def addcodehere(mc,h,k,l):
	x,y,z = h,k,l 
	
	stone = 1
	brick = 45
	air = 0
	wood_plank = 5
	glass_pane = 102
	stairs = 53
	fence = 85
	roof = 67
	roof_flat = 4
	mc.setBlocks(x-10, y, z-10, x+10, y+15, z+10, air)
	
	mc.setBlocks(x-7, y-1, z-7, x+7, y-3, z+7, stone)

	mc.setBlocks(x-7, y, z-7, x+7, y, z-7, fence)
	mc.setBlocks(x-7, y, z-7, x-7, y, z+7, fence)
	mc.setBlocks(x+7, y, z+7, x+7, y, z-7, fence)
	mc.setBlocks(x+7, y, z+7, x-7, y, z+7, fence)

	mc.setBlocks(x-5, y, z-5, x+5, y+9, z+5, brick)

	mc.setBlocks(x-4, y, z-4, x+4, y+8, z+4, air)

	mc.setBlocks(x-4, y-1, z-4, x+4, y-1, z+4, wood_plank)

	mc.setBlocks(x-4, y+4, z-4, x+4, y+4, z+4, wood_plank)

	mc.setBlocks(x-4, y+9, z-4, x+4, y+9, z+4, wood_plank)

	mc.setBlock(x-5, y, z, 64, 0)
	mc.setBlock(x-5, y+1, z, 64, 8)

	mc.setBlocks(x-5,y+1,z-2,x-5,y+2,z-3,glass_pane)
	mc.setBlocks(x-5,y+1,z+2,x-5,y+2,z+3,glass_pane)
	mc.setBlocks(x-5,y+6,z-2,x-5,y+7,z-3,glass_pane)
	mc.setBlocks(x-5,y+6,z+2,x-5,y+7,z+3,glass_pane)
	mc.setBlocks(x+5,y+1,z-2,x+5,y+2,z-3,glass_pane)
	mc.setBlocks(x+5,y+1,z+2,x+5,y+2,z+3,glass_pane)
	mc.setBlocks(x+5,y+6,z-2,x+5,y+7,z-3,glass_pane)
	mc.setBlocks(x+5,y+6,z+2,x+5,y+7,z+3,glass_pane)

	mc.setBlocks(x+3,y+4,z+4,x,y+4,z+3,air)

	for i in range(5):
		mc.setBlock(x+i-1,y+i,z+3,stairs)
		mc.setBlock(x+i-1,y+i,z+4,stairs)
	# add some banistairs too
	mc.setBlocks(x-1,y+5,z+4,x-1,y+5,z+2,fence)
	mc.setBlocks(x-1,y+5,z+2,x+2,y+5,z+2,fence)
	# Now lets add a roof
	# First the pitches
	for i in range(5):
		mc.setBlocks(x-5+i, y+10+i, z-5, x-5+i, y+10+i, z+5, roof)
		mc.setBlocks(x+5-i, y+10+i, z-5, x+5-i, y+10+i, z+5, roof)
	# Now the top
	mc.setBlocks(x, y+14, z-5, x, y+14, z+5, roof_flat)
	# Now the brick sides
	for i in range(4):
		mc.setBlocks(x-1-i, y+13-i, z-5, x+1+i, y+13-i, z-5, brick)
	for i in range(4):
		mc.setBlocks(x-1-i, y+13-i, z+5, x+1+i, y+13-i, z+5, brick)	




    
    
def main():
	mc = init()
	mc.player.setPos(0,0,0)  
	x,y,z = mc.player.getPos()
	#createSphere(10,mc)  
	#sphere(mc,x,y,z,5,1)
	x,y,z = 0,0,0
	check(mc,x,y,z)
	h,k,l = 0,10,0
	addcodehere(mc,h,k,l)
	mc.player.setPos(x+7 ,y+40,z+5)
	

if __name__ == "__main__":
	main()

"""
AIR                   0
STONE                 1
GRASS                 2
DIRT                  3
COBBLESTONE           4
WOOD_PLANKS           5
SAPLING               6
BEDROCK               7
WATER_FLOWING         8
WATER                 8
WATER_STATIONARY      9
LAVA_FLOWING         10
LAVA                 10
LAVA_STATIONARY      11
SAND                 12
GRAVEL               13
GOLD_ORE             14
IRON_ORE             15
COAL_ORE             16
WOOD                 17
LEAVES               18
GLASS                20
LAPIS_LAZULI_ORE     21
LAPIS_LAZULI_BLOCK   22
SANDSTONE            24
BED                  26
COBWEB               30
GRASS_TALL           31
WOOL                 35
FLOWER_YELLOW        37
FLOWER_CYAN          38
MUSHROOM_BROWN       39
MUSHROOM_RED         40
GOLD_BLOCK           41
IRON_BLOCK           42
STONE_SLAB_DOUBLE    43
STONE_SLAB           44
BRICK_BLOCK          45
TNT                  46
BOOKSHELF            47
MOSS_STONE           48
OBSIDIAN             49
TORCH                50
FIRE                 51
STAIRS_WOOD          53
CHEST                54
DIAMOND_ORE          56
DIAMOND_BLOCK        57
CRAFTING_TABLE       58
FARMLAND             60
FURNACE_INACTIVE     61
FURNACE_ACTIVE       62
DOOR_WOOD            64
LADDER               65
STAIRS_COBBLESTONE   67
DOOR_IRON            71
REDSTONE_ORE         73
SNOW                 78
ICE                  79
SNOW_BLOCK           80
CACTUS               81
CLAY                 82
SUGAR_CANE           83
FENCE                85
GLOWSTONE_BLOCK      89
BEDROCK_INVISIBLE    95
STONE_BRICK          98
GLASS_PANE          102
MELON               103
FENCE_GATE          107
GLOWING_OBSIDIAN    246
NETHER_REACTOR_CORE 247
"""
