# Base project format.
# Documentation https://github.com/raspberrypilearning/getting-started-with-minecraft-pi/blob/master/worksheet.md
from mcpi.minecraft import Minecraft
from mcpi import block
import sys

ip = "127.0.0.1"
mc = Minecraft.create(ip, 4711)
x,y,z = mc.player.getPos()
xyz=str(x)+","+str(y)+","+str(z)
mc.postToChat(xyz)           
 
from mcpi.minecraft import Minecraft
mc = Minecraft.create()

# Get our current position
x, y, z = mc.player.getPos()

# define our blocks we will be using
stone = 1
brick = 45
air = 0
wood_plank = 5
glass_pane = 102
stairs = 53
fence = 85
roof = 67
roof_flat = 4

# Now we build our house!
# first clear the area
mc.setBlocks(x-10, y, z-10, x+10, y+15, z+10, air)
# now lay a foundation
mc.setBlocks(x-7, y-1, z-7, x+7, y-3, z+7, stone)
# add add a fence around our plot
mc.setBlocks(x-7, y, z-7, x+7, y, z-7, fence)
mc.setBlocks(x-7, y, z-7, x-7, y, z+7, fence)
mc.setBlocks(x+7, y, z+7, x+7, y, z-7, fence)
mc.setBlocks(x+7, y, z+7, x-7, y, z+7, fence)
# now make our house block
mc.setBlocks(x-5, y, z-5, x+5, y+9, z+5, brick)
# and hollow it out
mc.setBlocks(x-4, y, z-4, x+4, y+8, z+4, air)
# make the ground floor
mc.setBlocks(x-4, y-1, z-4, x+4, y-1, z+4, wood_plank)
# make the first floor
mc.setBlocks(x-4, y+4, z-4, x+4, y+4, z+4, wood_plank)
# make the ceiling
mc.setBlocks(x-4, y+9, z-4, x+4, y+9, z+4, wood_plank)
# make the door - 64 is a wood door, id 0 is the bootom, id 8 is the top part
mc.setBlock(x-5, y, z, 64, 0)
mc.setBlock(x-5, y+1, z, 64, 8)
# Add some windows
mc.setBlocks(x-5,y+1,z-2,x-5,y+2,z-3,glass_pane)
mc.setBlocks(x-5,y+1,z+2,x-5,y+2,z+3,glass_pane)
mc.setBlocks(x-5,y+6,z-2,x-5,y+7,z-3,glass_pane)
mc.setBlocks(x-5,y+6,z+2,x-5,y+7,z+3,glass_pane)
mc.setBlocks(x+5,y+1,z-2,x+5,y+2,z-3,glass_pane)
mc.setBlocks(x+5,y+1,z+2,x+5,y+2,z+3,glass_pane)
mc.setBlocks(x+5,y+6,z-2,x+5,y+7,z-3,glass_pane)
mc.setBlocks(x+5,y+6,z+2,x+5,y+7,z+3,glass_pane)
# Now add the stairs
# Create a hole in the floor first
mc.setBlocks(x+3,y+4,z+4,x,y+4,z+3,air)
# Now loop through our steps
for i in range(5):
    mc.setBlock(x+i-1,y+i,z+3,stairs)
    mc.setBlock(x+i-1,y+i,z+4,stairs)
# add some banistairs too
mc.setBlocks(x-1,y+5,z+4,x-1,y+5,z+2,fence)
mc.setBlocks(x-1,y+5,z+2,x+2,y+5,z+2,fence)
# Now lets add a roof
# First the pitches
for i in range(5):
    mc.setBlocks(x-5+i, y+10+i, z-5, x-5+i, y+10+i, z+5, roof)
    mc.setBlocks(x+5-i, y+10+i, z-5, x+5-i, y+10+i, z+5, roof, 1)
# Now the top
mc.setBlocks(x, y+14, z-5, x, y+14, z+5, roof_flat)
# Now the brick sides
for i in range(4):
    mc.setBlocks(x-1-i, y+13-i, z-5, x+1+i, y+13-i, z-5, brick)
for i in range(4):
    mc.setBlocks(x-1-i, y+13-i, z+5, wx+1+i, y+13-i, z+5, brick)

mc.setBlock(h,k,l,35,0)
mc.setBlock(h,k+1,l,35,1)
mc.setBlock(h,k+2,l,35,2)
mc.setBlock(h,k+3,l,35,3)
mc.setBlock(h,k+4,l,35,4)
mc.setBlock(h,k+5,l,35,5)
mc.setBlock(h,k+6,l,35,6)
mc.setBlock(h,k+7,l,35,7)
mc.setBlock(h,k+8,l,35,8)
mc.setBlock(h,k+9,l,35,9)
mc.setBlock(h,k+10,l,35,10)
mc.setBlock(h,k+11,l,35,11)
mc.setBlock(h,k+12,l,35,12)
mc.setBlock(h,k+13,l,35,13)
mc.setBlock(h,k+14,l,35,14)
mc.setBlock(h,k+15,l,41)
mc.player.setPos(h,k+17,l)
mc.setBlock(0,30,0,3)
mc.setBlock(0,30,1,3)
mc.setBlock(1,30,0,3)
mc.setBlock(1,30,1,3)
mc.setBlock(-1,30,0,3)
mc.setBlock(-1,30,1,3)
mc.player.setPos(1,32,1)   


'''
#API Blocks
#====================
#   AIR                   0
#   STONE                 1
#   GRASS                 2
#   DIRT                  3
#   COBBLESTONE           4
#   WOOD_PLANKS           5
#   SAPLING               6
#   BEDROCK               7
#   WATER_FLOWING         8
#   WATER                 8
#   WATER_STATIONARY      9
#   LAVA_FLOWING         10
#   LAVA                 10
#   LAVA_STATIONARY      11
#   SAND                 12
#   GRAVEL               13
#   GOLD_ORE             14
#   IRON_ORE             15
#   COAL_ORE             16
#   WOOD                 17
#   LEAVES               18
#   GLASS                20
#   LAPIS_LAZULI_ORE     21
#   LAPIS_LAZULI_BLOCK   22
#   SANDSTONE            24
#   BED                  26
#   COBWEB               30
#   GRASS_TALL           31
#   WOOL                 35
#   FLOWER_YELLOW        37
#   FLOWER_CYAN          38
#   MUSHROOM_BROWN       39
#   MUSHROOM_RED         40
#   GOLD_BLOCK           41
#   IRON_BLOCK           42
#   STONE_SLAB_DOUBLE    43
#   STONE_SLAB           44
#   BRICK_BLOCK          45
#   TNT                  46
#   BOOKSHELF            47
#   MOSS_STONE           48
#   OBSIDIAN             49
#   TORCH                50
#   FIRE                 51
#   STAIRS_WOOD          53
#   CHEST                54
#   DIAMOND_ORE          56
#   DIAMOND_BLOCK        57
#   CRAFTING_TABLE       58
#   FARMLAND             60
#   FURNACE_INACTIVE     61
#   FURNACE_ACTIVE       62
#   DOOR_WOOD            64
#   LADDER               65
#   STAIRS_COBBLESTONE   67
#   DOOR_IRON            71
#   REDSTONE_ORE         73
#   SNOW                 78
#   ICE                  79
#   SNOW_BLOCK           80
#   CACTUS               81
#   CLAY                 82
#   SUGAR_CANE           83
#   FENCE                85
#   GLOWSTONE_BLOCK      89
#   BEDROCK_INVISIBLE    95
#   STONE_BRICK          98
#   GLASS_PANE          102
#   MELON               103
#   FENCE_GATE          107
#   GLOWING_OBSIDIAN    246
#   NETHER_REACTOR_CORE 247
'''



